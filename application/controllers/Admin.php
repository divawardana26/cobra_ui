<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
    }

    public function index()
    {
        $data['title'] = 'Dashboard';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/index', $data);
        $this->load->view('templates/footer');
    }


    public function role()
    {
        $data['title'] = 'Role';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get('user_role')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role', $data);
        $this->load->view('templates/footer');
    }


    public function roleAccess($role_id)
    {
        $data['title'] = 'Role Access';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $data['role'] = $this->db->get_where('user_role', ['id' => $role_id])->row_array();

        $this->db->where('id !=', 1);
        $data['menu'] = $this->db->get('user_menu')->result_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/role-access', $data);
        $this->load->view('templates/footer');
    }


    public function changeAccess()
    {
        $menu_id = $this->input->post('menuId');
        $role_id = $this->input->post('roleId');

        $data = [
            'role_id' => $role_id,
            'menu_id' => $menu_id
        ];

        $result = $this->db->get_where('user_access_menu', $data);

        if ($result->num_rows() < 1) {
            $this->db->insert('user_access_menu', $data);
        } else {
            $this->db->delete('user_access_menu', $data);
        }

        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Access Changed!</div>');
    }

    public function master()
    {
        $data['title'] = 'Master Data';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/master', $data);
        $this->load->view('templates/footer');
    }
//SISWA
    public function siswa()
    {
        $data['title'] = 'Master Data';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->model('Kelas_m');
        $data['kelas'] = $this->db->get('kelas')->result_array();
        $this->load->model('Jurusan_m');
        $data['jurusan'] = $this->db->get('jurusan')->result_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/siswa', $data);
        $this->load->view('templates/footer');
    }

    public function list_siswa()
	{
        $this->load->model('Siswa_m');
		$list = $this->Siswa_m->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $Siswa_m) {
			$no++;
			$row = array();
			$row[] = $Siswa_m->nis;
			$row[] = $Siswa_m->nisn;
			$row[] = $Siswa_m->nama_siswa;
			$row[] = $Siswa_m->j_kelamin;
			$row[] = $Siswa_m->id_kelas;
			$row[] = $Siswa_m->id_jurusan;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_siswa('."'".$Siswa_m->id."'".')"><i class="fa fa-pen"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_siswa('."'".$Siswa_m->id."'".')"><i class="fa fa-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->Siswa_m->count_all(),
						"recordsFiltered" => $this->Siswa_m->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function edit_siswa($id)
	{
        $this->load->model('Siswa_m');
		$data = $this->Siswa_m->get_by_id($id);
		echo json_encode($data);
	}

	public function tambah_siswa()
	{
        $this->load->model('Siswa_m');
		$data = array(
				'nis' => $this->input->post('nis'),
				'nisn' => $this->input->post('nisn'),
				'nama_siswa' => $this->input->post('namaSiswa'),
				'j_kelamin' => $this->input->post('jenisKelamin'),
				'id_kelas' => $this->input->post('kelas'),
				'id_jurusan' => $this->input->post('jurusan'),
			);
		$insert = $this->Siswa_m->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function update_siswa()
	{
        $this->load->model('Siswa_m');
		$data = array(
				'nis' => $this->input->post('nis'),
				'nisn' => $this->input->post('nisn'),
				'nama_siswa' => $this->input->post('namaSiswa'),
				'j_kelamin' => $this->input->post('jenisKelamin'),
				'id_kelas' => $this->input->post('kelas'),
				'id_jurusan' => $this->input->post('jurusan'),
			);
		$this->Siswa_m->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function delete_siswa($id)
	{
        $this->load->model('Siswa_m');
		$this->Siswa_m->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}
//END SISWA

//GURU
    public function guru()
    {
        $data['title'] = 'Master Data';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/guru', $data);
        $this->load->view('templates/footer');
    }

    public function list_guru()
    {
        $this->load->model('Guru_m');
        $list = $this->Guru_m->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $Guru_m) {
            $no++;
            $row = array();
            $row[] = $Guru_m->kode_guru;
            $row[] = $Guru_m->nip;
            $row[] = $Guru_m->nama_guru;
            $row[] = $Guru_m->tempat_lahir .",". $Guru_m->tanggal_lahir;
            $row[] = $Guru_m->j_kelamin;
            $row[] = $Guru_m->alamat;
            $row[] = $Guru_m->no_telp;
            $row[] = $Guru_m->jabatan;

            //add html for action
            $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_guru('."'".$Guru_m->id_guru."'".')"><i class="fa fa-pen"></i></a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_guru('."'".$Guru_m->id_guru."'".')"><i class="fa fa-trash"></i></a>';
        
            $data[] = $row;
        }

        $output = array(
                        "draw" => $_POST['draw'],
                        "recordsTotal" => $this->Guru_m->count_all(),
                        "recordsFiltered" => $this->Guru_m->count_filtered(),
                        "data" => $data,
                );
        //output to json format
        echo json_encode($output);
    }

    public function edit_guru($id_guru)
    {
        $this->load->model('Guru_m');
        $data = $this->Guru_m->get_by_id($id_guru);
        echo json_encode($data);
    }

    public function tambah_guru()
    {
        $this->load->model('Guru_m');
        $data = array(
                'kode_guru' => $this->input->post('kodeGuru'),
                'nip' => $this->input->post('nip'),
                'nama_guru' => $this->input->post('namaGuru'),
                'tempat_lahir' => $this->input->post('tempatLahir'),
                'tanggal_lahir' => $this->input->post('tanggalLahir'),
                'j_kelamin' => $this->input->post('jenisKelamin'),
                'alamat' => $this->input->post('alamat'),
                'no_telp' => $this->input->post('noTelpon'),
                'jabatan' => $this->input->post('jabatan'),
            );
        $insert = $this->Guru_m->save($data);
        echo json_encode(array("status" => TRUE));
    }

    public function update_guru()
    {
        $this->load->model('Guru_m');
        $data = array(

                'kode_guru' => $this->input->post('kodeGuru'),
                'nip' => $this->input->post('nip'),
                'nama_guru' => $this->input->post('namaGuru'),
                'tempat_lahir' => $this->input->post('tempatLahir'),
                'tanggal_lahir' => $this->input->post('tanggalLahir'),
                'j_kelamin' => $this->input->post('jenisKelamin'),
                'alamat' => $this->input->post('alamat'),
                'no_telp' => $this->input->post('noTelpon'),
                'jabatan' => $this->input->post('jabatan'),
            );
        $this->Guru_m->update(array('id_guru' => $this->input->post('id_guru')), $data);
        echo json_encode(array("status" => TRUE));
    }

    public function delete_guru($id_guru)
    {
        $this->load->model('Guru_m');
        $this->Guru_m->delete_by_id($id_guru);
        echo json_encode(array("status" => TRUE));
    }
//END GURU

//KELAS
public function kelas()
{
    $this->load->model('Kelas_m');
    $data['title'] = 'Master Data';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('admin/kelas', $data);
    $this->load->view('templates/footer');
}

public function list_kelas()
{
    $this->load->model('Kelas_m');
    $list = $this->Kelas_m->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $Kelas_m) {
        $no++;
        $row = array();
        $row[] = $Kelas_m->nama_kelas;

        //add html for action
        $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_kelas('."'".$Kelas_m->id_kelas."'".')"><i class="fa fa-pen"></i></a>
              <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_kelas('."'".$Kelas_m->id_kelas."'".')"><i class="fa fa-trash"></i></a>';
    
        $data[] = $row;
    }

    $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->Kelas_m->count_all(),
                    "recordsFiltered" => $this->Kelas_m->count_filtered(),
                    "data" => $data,
            );
    //output to json format
    echo json_encode($output);
}

public function edit_kelas($id_kelas)
{
    $this->load->model('Kelas_m');
    $data = $this->Kelas_m->get_by_id($id_kelas);
    echo json_encode($data);
}

public function tambah_kelas()
{
    $this->load->model('Kelas_m');
    $data = array(
            'nama_kelas' => $this->input->post('kelas'),
        );
    $insert = $this->Kelas_m->save($data);
    echo json_encode(array("status" => TRUE));
}

public function update_kelas()
{
    $this->load->model('Kelas_m');
    $data = array(
            'nama_kelas' => $this->input->post('kelas'),
        );
    $this->Kelas_m->update(array('id_kelas' => $this->input->post('id_kelas')), $data);
    echo json_encode(array("status" => TRUE));
}

public function delete_kelas($id_kelas)
{
    $this->load->model('Kelas_m');
    $this->Kelas_m->delete_by_id($id_kelas);
    echo json_encode(array("status" => TRUE));
}
//END KELAS

//JURUSAN
public function jurusan()
{
    $this->load->model('Jurusan_m');
    $data['title'] = 'Master Data';
    $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

    $this->load->view('templates/header', $data);
    $this->load->view('templates/sidebar', $data);
    $this->load->view('templates/topbar', $data);
    $this->load->view('admin/jurusan', $data);
    $this->load->view('templates/footer');
}

public function list_jurusan()
{
    $this->load->model('Jurusan_m');
    $list = $this->Jurusan_m->get_datatables();
    $data = array();
    $no = $_POST['start'];
    foreach ($list as $Jurusan_m) {
        $no++;
        $row = array();
        $row[] = $Jurusan_m->nama_jurusan;

        //add html for action
        $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_jurusan('."'".$Jurusan_m->id_jurusan."'".')"><i class="fa fa-pen"></i></a>
              <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_jurusan('."'".$Jurusan_m->id_jurusan."'".')"><i class="fa fa-trash"></i></a>';
    
        $data[] = $row;
    }

    $output = array(
                    "draw" => $_POST['draw'],
                    "recordsTotal" => $this->Jurusan_m->count_all(),
                    "recordsFiltered" => $this->Jurusan_m->count_filtered(),
                    "data" => $data,
            );
    //output to json format
    echo json_encode($output);
}

public function edit_jurusan($id_jurusan)
{
    $this->load->model('Jurusan_m');
    $data = $this->Jurusan_m->get_by_id($id_jurusan);
    echo json_encode($data);
}

public function tambah_jurusan()
{
    $this->load->model('Jurusan_m');
    $data = array(
            'nama_jurusan' => $this->input->post('jurusan'),
        );
    $insert = $this->Jurusan_m->save($data);
    echo json_encode(array("status" => TRUE));
}

public function update_jurusan()
{
    $this->load->model('Jurusan_m');
    $data = array(
            'nama_jurusan' => $this->input->post('jurusan'),
        );
    $this->Jurusan_m->update(array('id_jurusan' => $this->input->post('id_jurusan')), $data);
    echo json_encode(array("status" => TRUE));
}

public function delete_jurusan($id_jurusan)
{
    $this->load->model('Jurusan_m');
    $this->Jurusan_m->delete_by_id($id_jurusan);
    echo json_encode(array("status" => TRUE));
}
//END JURUSAN
}
