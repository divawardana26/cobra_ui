<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export extends CI_Controller {

// Load database
 public function __construct() {
 parent::__construct();
 $this->load->model('Export_m');
 }

public function index() {
 $data = array( 'title' => 'Data user',
 'user' => $this->Export_m->listing());
 $this->load->view('excel',$data);
 }

public function export_siswa(){
 $data = array( 'title' => 'Data Siswa',
 'user' => $this->Export_m->siswa());
 $this->load->view('excel/excel_siswa',$data);
 }

 public function export_guru(){
 $data = array( 'title' => 'Data Guru',
 'user' => $this->Export_m->guru());
 $this->load->view('excel/excel_guru',$data);
 }

}

/* End of file Export.php */
/* Location: ./application/controllers/Export.php */