<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MasterData extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('MasterData_m','m');
    }

    public function index()
    {
        $data['title'] = 'Siswa';
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();

        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('admin/siswa', $data);
        $this->load->view('templates/footer');
    }

    public function ajax_list()
	{
		$list = $this->m->get_datatables();
		$data = array();
		$no = $_POST['start'];
		foreach ($list as $m) {
			$no++;
			$row = array();
			$row[] = $m->nis;
			$row[] = $m->nisn;
			$row[] = $m->nama_siswa;
			$row[] = $m->j_kelamin;
			$row[] = $m->id_kelas;
			$row[] = $m->id_jurusan;

			//add html for action
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_person('."'".$m->id."'".')"><i class="fa fa-pen"></i></a>
				  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_person('."'".$m->id."'".')"><i class="fa fa-trash"></i></a>';
		
			$data[] = $row;
		}

		$output = array(
						"draw" => $_POST['draw'],
						"recordsTotal" => $this->m->count_all(),
						"recordsFiltered" => $this->m->count_filtered(),
						"data" => $data,
				);
		//output to json format
		echo json_encode($output);
	}

	public function ajax_edit($id)
	{
		$data = $this->m->get_by_id($id);
		echo json_encode($data);
	}

	public function ajax_add()
	{
		$data = array(
				'nis' => $this->input->post('nis'),
				'nisn' => $this->input->post('nisn'),
				'nama_siswa' => $this->input->post('namaSiswa'),
				'j_kelamin' => $this->input->post('jenisKelamin'),
				'id_kelas' => $this->input->post('kelas'),
				'id_jurusan' => $this->input->post('jurusan'),
			);
		$insert = $this->m->save($data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_update()
	{
		$data = array(
				'nis' => $this->input->post('nis'),
				'nisn' => $this->input->post('nisn'),
				'nama_siswa' => $this->input->post('namaSiswa'),
				'j_kelamin' => $this->input->post('jenisKelamin'),
				'id_kelas' => $this->input->post('kelas'),
				'id_jurusan' => $this->input->post('jurusan'),
			);
		$this->m->update(array('id' => $this->input->post('id')), $data);
		echo json_encode(array("status" => TRUE));
	}

	public function ajax_delete($id)
	{
		$this->m->delete_by_id($id);
		echo json_encode(array("status" => TRUE));
	}

}
