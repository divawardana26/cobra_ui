<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_m extends CI_Model {

public function __construct()
 {
 parent::__construct();
 $this->load->database();
 }

// Listing
 public function siswa() {
 $this->db->select('*');
 $this->db->from('siswa');
 $query = $this->db->get();
 return $query->result();
 }

 public function guru() {
 $this->db->select('*');
 $this->db->from('guru');
 $query = $this->db->get();
 return $query->result();
 }

}

/* End of file Export_m.php */
/* Location: ./application/models/Export_m.php */