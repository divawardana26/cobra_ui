

<!-- Begin Page Content -->
<div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Guru</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <button class="btn btn-success" onclick="tambah_guru()"><i class="fa fa-plus"></i> Tambah Guru</button>
              <button class="btn btn-info" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>
              <div class="dropdown" >
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Excel
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a href="<?php echo base_url('export/export_guru')?>" class="dropdown-item">Export</a>
                  <a href="" class="dropdown-item">Import</a>
                </div>
              </div>
              <br>
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>Kode Guru</th>
                    <th>NIP</th>
                    <th>Nama Guru</th>
                    <th>Tempat, Tanggal Lahir</th>
                    <th>L/P</th>
                    <th>Alamat</th>
                    <th>No. Telpon</th>
                    <th>Jabatan</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                    <th>Kode Guru</th>
                    <th>NIP</th>
                    <th>Nama Guru</th>
                    <th>Tempat, Tanggal Lahir</th>
                    <th>L/P</th>
                    <th>Alamat</th>
                    <th>No. Telpon</th>
                    <th>Jabatan</th>
                    <th>Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Form Guru</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_guru"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Kode Guru*</label>
                            <div class="col-md-12">
                                <input name="kodeGuru" placeholder="Kode Guru" class="form-control" type="text" id="kodeGuru" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NIP*</label>
                            <div class="col-md-12">
                                <input name="nip" placeholder="NIP" class="form-control" type="text" id="nip">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Nama Guru*</label>
                            <div class="col-md-12">
                                <input name="namaGuru" placeholder="Nama Guru" class="form-control" type="text" id="nama">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tempat Lahir*</label>
                            <div class="col-md-12">
                                <input name="tempatLahir" placeholder="Tempat Lahir" class="form-control" type="text" id="tempatLahir">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Tanggal Lahir*</label>
                            <div class="col-md-12">
                                <input name="tanggalLahir" placeholder="" class="form-control" type="date" id="tanggalLahir">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Jenis Kelamin*</label>
                            <div class="col-md-12">
                                <select name="jenisKelamin" class="form-control" id="jk">
                                    <option value="">--Jenis Kelamin--</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Alamat*</label>
                            <div class="col-md-12">
                                <textarea name="alamat" placeholder="Alamat" class="form-control" type="text" id="alamat"></textarea>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">No. Telpon*</label>
                            <div class="col-md-12">
                                <input name="noTelpon" placeholder="No. Telpon" class="form-control" type="text" id="noTelpon">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Jabatan*</label>
                            <div class="col-md-12">
                                <input name="jabatan" placeholder="Jabatan" class="form-control" type="text" id="jabatan">
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/css/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url('assets/')?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/list_guru')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});



function tambah_guru()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Tambah Guru'); // Set Title to Bootstrap modal title
}

function edit_guru(id_guru)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('Admin/edit_guru/')?>/" + id_guru,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_guru"]').val(data.id_guru);
            $('[name="kodeGuru"]').val(data.kode_guru);
            $('[name="nip"]').val(data.nip);
            $('[name="namaGuru"]').val(data.nama_guru);
            $('[name="tempatLahir"]').val(data.tempat_lahir);
            $('[name="tanggalLahir"]').val(data.tanggal_lahir);
            $('[name="jenisKelamin"]').val(data.j_kelamin);
            $('[name="alamat"]').val(data.alamat);
            $('[name="noTelpon"]').val(data.no_telp);
            $('[name="jabatan"]').val(data.jabatan);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Form Ubah Siswa'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
      var kodeGuru = document.getElementById("kodeGuru").value;
      var nip = document.getElementById("nip").value;
      var nama = document.getElementById("nama").value;
      var tempatLahir = document.getElementById("tempatLahir").value;
      var tanggalLahir = document.getElementById("tanggalLahir").value;
      var jk = document.getElementById("jk").value;
      var alamat = document.getElementById("alamat").value;
      var noTelpon = document.getElementById("noTelpon").value;
      var jabatan = document.getElementById("jabatan").value;

      if (kodeGuru != "" && nip != "" && nama != "" && tempatLahir != "" && tanggalLahir != "" && jk != "" && alamat != "" && noTelpon != "" && jabatan != ""){
        url = "<?php echo site_url('Admin/tambah_guru')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }

        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Tambah Guru',
                    text: 'Anda Berhasil Menambah Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });

    } else {

      var kodeGuru = document.getElementById("kodeGuru").value;
      var nip = document.getElementById("nip").value;
      var nama = document.getElementById("nama").value;
      var tempatLahir = document.getElementById("tempatLahir").value;
      var tanggalLahir = document.getElementById("tanggalLahir").value;
      var jk = document.getElementById("jk").value;
      var alamat = document.getElementById("alamat").value;
      var noTelpon = document.getElementById("noTelpon").value;
      var jabatan = document.getElementById("jabatan").value;

      if (kodeGuru != "" && nip != "" && nama != "" && tempatLahir != "" && tanggalLahir != "" && jk != "" && alamat != "" && noTelpon != "" && jabatan != ""){
        url = "<?php echo site_url('Admin/update_Guru')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }
        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Edit Guru',
                    text: 'Anda Berhasil Edit Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
    }

    
}

function delete_guru(id_guru)
{
  swal({
  title: "Apakah Anda yakin?",
  text: "Anda tidak akan dapat memulihkan data ini!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Ya, hapus!",
  cancelButtonText: "Tidak, batal!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    $.ajax({
            url : "<?php echo site_url('Admin/delete_guru')?>/"+id_guru,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    swal("Batal", "Data anda aman :)", "error");
  }
});
}

</script>
