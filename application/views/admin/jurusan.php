
<!-- Begin Page Content -->
<div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Jurusan</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <button class="btn btn-success" onclick="tambah_jurusan()"><i class="fa fa-plus"></i> Tambah Jurusan</button>
              <button class="btn btn-info" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>
              <br>
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>Jurusan</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                    <th>Jurusan</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Form Jurusan</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_jurusan"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Jurusan*</label>
                            <div class="col-md-12">
                                <input name="jurusan" placeholder="Jurusan" class="form-control" type="text" id="jurusan" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/css/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url('assets/')?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/list_jurusan')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});



function tambah_jurusan()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Tambah Jurusan'); // Set Title to Bootstrap modal title
}

function edit_jurusan(id_jurusan)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('Admin/edit_jurusan/')?>/" + id_jurusan,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_jurusan"]').val(data.id_jurusan);
            $('[name="jurusan"]').val(data.nama_jurusan);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Form Ubah Jurusan'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
      var jurusan = document.getElementById("jurusan").value;

      if (jurusan != ""){
        url = "<?php echo site_url('Admin/tambah_jurusan')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }

        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Tambah Jurusan',
                    text: 'Anda Berhasil Menambah Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });

    } else {

      var nis = document.getElementById("jurusan").value;

      if (nis != ""){
        url = "<?php echo site_url('Admin/update_jurusan')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }
        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Edit Jurusan',
                    text: 'Anda Berhasil Edit Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
    }

    
}

function delete_jurusan(id_jurusan)
{
  swal({
  title: "Apakah Anda yakin?",
  text: "Anda tidak akan dapat memulihkan data ini!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Ya, hapus!",
  cancelButtonText: "Tidak, batal!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    $.ajax({
            url : "<?php echo site_url('Admin/delete_jurusan')?>/"+id_jurusan,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    swal("Batal", "Data anda aman :)", "error");
  }
});
}

</script>
