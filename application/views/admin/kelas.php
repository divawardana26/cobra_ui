
<!-- Begin Page Content -->
<div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Kelas</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <button class="btn btn-success" onclick="tambah_kelas()"><i class="fa fa-plus"></i> Tambah Kelas</button>
              <button class="btn btn-info" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>
              <br>
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>Kelas</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                    <th>Kelas</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Form Kelas</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id_kelas"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">Kelas*</label>
                            <div class="col-md-12">
                                <input name="kelas" placeholder="Kelas" class="form-control" type="text" id="kelas" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/css/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url('assets/')?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/list_kelas')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});



function tambah_kelas()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Tambah Kelas'); // Set Title to Bootstrap modal title
}

function edit_kelas(id_kelas)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('Admin/edit_kelas/')?>/" + id_kelas,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id_kelas"]').val(data.id_kelas);
            $('[name="kelas"]').val(data.nama_kelas);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Form Ubah Kelas'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
      var kelas = document.getElementById("kelas").value;

      if (kelas != ""){
        url = "<?php echo site_url('Admin/tambah_kelas')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }

        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Tambah Kelas',
                    text: 'Anda Berhasil Menambah Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });

    } else {

      var nis = document.getElementById("kelas").value;

      if (nis != ""){
        url = "<?php echo site_url('Admin/update_kelas')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }
        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Edit Kelas',
                    text: 'Anda Berhasil Edit Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
    }

    
}

function delete_kelas(id_kelas)
{
  swal({
  title: "Apakah Anda yakin?",
  text: "Anda tidak akan dapat memulihkan data ini!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Ya, hapus!",
  cancelButtonText: "Tidak, batal!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    $.ajax({
            url : "<?php echo site_url('Admin/delete_kelas')?>/"+id_kelas,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    swal("Batal", "Data anda aman :)", "error");
  }
});
}

</script>
