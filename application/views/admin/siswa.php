
<!-- Begin Page Content -->
<div class="container-fluid">
          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Tabel Siswa</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
              <button class="btn btn-success" onclick="tambah_siswa()"><i class="fa fa-plus"></i> Tambah Siswa</button>
              <button class="btn btn-info" onclick="reload_table()"><i class="fa fa-refresh"></i> Reload</button>
              <div class="dropdown" >
                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  Excel
                </button>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                  <a href="<?php echo base_url('export/export_siswa')?>" class="dropdown-item">Export</a>
                  <a href="javascript:void(0)" id="import" onclick="import_siswa()"  class="dropdown-item">Import</a>
                </div>
              </div>
              <br>
                <table class="table table-bordered" id="table" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                    <th>NIS</th>
                    <th>NISN</th>
                    <th>Nama Siswa</th>
                    <th>L/P</th>
                    <th>Kelas</th>
                    <th>Jurusan</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                  </tbody>
                  <tfoot>
                    <tr>
                    <th>NIS</th>
                    <th>NISN</th>
                    <th>Nama Siswa</th>
                    <th>L/P</th>
                    <th>Kelas</th>
                    <th>Jurusan</th>
                    <th style="width:125px;">Action</th>
                    </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>

        </div>

        <!-- Bootstrap modal -->
<div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Form Siswa</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="#" id="form" class="form-horizontal">
                    <input type="hidden" value="" name="id"/> 
                    <div class="form-body">
                        <div class="form-group">
                            <label class="control-label col-md-3">NIS*</label>
                            <div class="col-md-12">
                                <input name="nis" placeholder="NIS" class="form-control" type="text" id="nis" >
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">NISN*</label>
                            <div class="col-md-12">
                                <input name="nisn" placeholder="NISN" class="form-control" type="text" id="nisn">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Nama Siswa*</label>
                            <div class="col-md-12">
                                <input name="namaSiswa" placeholder="Nama Siswa" class="form-control" type="text" id="nama">
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Jenis Kelamin*</label>
                            <div class="col-md-12">
                                <select name="jenisKelamin" class="form-control" id="jk">
                                    <option value="">--Jenis Kelamin--</option>
                                    <option value="L">L</option>
                                    <option value="P">P</option>
                                </select>
                                <span class="help-block"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kelas*</label>
                            <div class="col-md-12">
                        <select name="kelas" id="kelas" class="form-control">
                            <option value="">Select Kelas</option>
                            <?php foreach ($kelas as $m) : ?>
                            <option value="<?= $m['nama_kelas']; ?>"><?= $m['nama_kelas']; ?></option>
                            <?php endforeach; ?>
                        </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Jurusan*</label>
                            <div class="col-md-12">
                        <select name="jurusan" id="jurusan" class="form-control">
                            <option value="">Select Jurusan</option>
                            <?php foreach ($jurusan as $m) : ?>
                            <option value="<?= $m['nama_jurusan']; ?>"><?= $m['nama_jurusan']; ?></option>
                            <?php endforeach; ?>
                        </select>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Bootstrap modal -->
<div class="modal fade" id="modal_form_import" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title">Form Import Siswa</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body form">
                <form action="<?php echo base_url();?>Import/import/" method="post" enctype="multipart/form-data" id="form" class="form-horizontal" method="post">
                  <a href="<?php echo base_url("excel/format.xlsx"); ?>">Download Format</a><br>
                    <input type="file" name="file" required accept=".xls, .xlsx">
                    <button type="submit" name="import" id="btnImport" onclick="" class="btn btn-primary">Import</button>
                </form>
            </div>
            <!-- <div class="modal-footer">
                <button type="submit" name="import" id="btnImport" onclick="" class="btn btn-primary">Import</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
            </div> -->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<link href="<?php echo base_url();?>assets/css/sweetalert.css" rel="stylesheet">
<script src="<?php echo base_url('assets/')?>js/jquery-3.1.1.min.js"></script>
<script src="<?php echo base_url('assets/js/sweetalert.min.js')?>"></script>
<script type="text/javascript">

var save_method; //for save method string
var table;

$(document).ready(function() {

    //datatables
    table = $('#table').DataTable({ 

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.

        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo site_url('Admin/list_siswa')?>",
            "type": "POST"
        },

        //Set column definition initialisation properties.
        "columnDefs": [
        { 
            "targets": [ -1 ], //last column
            "orderable": false, //set not orderable
        },
        ],

    });

});



function tambah_siswa()
{
    save_method = 'add';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Tambah Siswa'); // Set Title to Bootstrap modal title
}

function edit_siswa(id)
{
    save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string

    //Ajax Load data from ajax
    $.ajax({
        url : "<?php echo site_url('Admin/edit_siswa/')?>/" + id,
        type: "GET",
        dataType: "JSON",
        success: function(data)
        {

            $('[name="id"]').val(data.id);
            $('[name="nis"]').val(data.nis);
            $('[name="nisn"]').val(data.nisn);
            $('[name="namaSiswa"]').val(data.nama_siswa);
            $('[name="jenisKelamin"]').val(data.j_kelamin);
            $('[name="kelas"]').val(data.id_kelas);
            $('[name="jurusan"]').val(data.id_jurusan);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Form Ubah Siswa'); // Set title to Bootstrap modal title

        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            alert('Error get data from ajax');
        }
    });
}

function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}

function save()
{
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;

    if(save_method == 'add') {
      var nis = document.getElementById("nis").value;
      var nisn = document.getElementById("nisn").value;
      var nama = document.getElementById("nama").value;
      var jk = document.getElementById("jk").value;
      var kelas = document.getElementById("kelas").value;
      var jurusan = document.getElementById("jurusan").value;

      if (nis != "" && nisn != "" && nama != "" && jk != "" && kelas != "" && jurusan != ""){
        url = "<?php echo site_url('Admin/tambah_siswa')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }

        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Tambah Siswa',
                    text: 'Anda Berhasil Menambah Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });

    } else {

      var nis = document.getElementById("nis").value;
      var nisn = document.getElementById("nisn").value;
      var nama = document.getElementById("nama").value;
      var jk = document.getElementById("jk").value;
      var kelas = document.getElementById("kelas").value;
      var jurusan = document.getElementById("jurusan").value;

      if (nis != "" && nisn != "" && nama != "" && jk != "" && kelas != "" && jurusan != ""){
        url = "<?php echo site_url('Admin/update_siswa')?>";
      }else{
        swal("Peringatan", "Semua data harus diisi :)", "warning");
      }
        // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#form').serialize(),
        dataType: "JSON",
        success: function(data)
        {

            if(data.status) //if success close modal and reload ajax table
            {
              swal({
                    type: 'success',
                    title: 'Edit Siswa',
                    text: 'Anda Berhasil Edit Data'
                  });
                $('#modal_form').modal('hide');
                reload_table();
            }

            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 


        },
        error: function (jqXHR, textStatus, errorThrown)
        {
          swal("Peringatan", "Semua data harus diisi :)", "warning");
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 

        }
    });
    }

    
}

function delete_siswa(id)
{
  swal({
  title: "Apakah Anda yakin?",
  text: "Anda tidak akan dapat memulihkan data ini!",
  type: "warning",
  showCancelButton: true,
  confirmButtonColor: "#DD6B55",
  confirmButtonText: "Ya, hapus!",
  cancelButtonText: "Tidak, batal!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm){
  if (isConfirm) {
    $.ajax({
            url : "<?php echo site_url('Admin/delete_siswa')?>/"+id,
            type: "POST",
            dataType: "JSON",
            success: function(data)
            {
                //if success reload ajax table
                      swal(
                        'Hapus',
                        'Berhasil Terhapus',
                        'success'
                      )
                $('#modal_form').modal('hide');
                reload_table();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                alert('Error deleting data');
            }
        });
  } else {
    swal("Batal", "Data anda aman :)", "error");
  }
});
}

function import_siswa()
{
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $('#modal_form_import').modal('show'); // show bootstrap modal
    $('.modal-title').text('Form Import Siswa'); // Set Title to Bootstrap modal title
}
</script>
