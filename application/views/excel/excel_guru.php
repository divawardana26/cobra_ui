<?php 

header("Content-type: application/octet-stream");

header("Content-Disposition: attachment; filename=$title.xls");

header("Pragma: no-cache");

header("Expires: 0");

?>

<table border="1" width="50%">

<thead>

<tr>

 <th>Kode Guru</th>

 <th>NIP</th>

 <th>Nama Guru</th>

 <th>Tempat, Tanggal Lahir</th>

 <th>L/P</th>

 <th>Alamat</th>

 <th>No. Telpon</th>

 <th>Jabatan</th>
 </tr>

</thead>

<tbody>

<?php $i=1; foreach($user as $user) { ?>

<tr>

 <td><?php echo $user->kode_guru ?></td>

 <td><?php echo $user->nip ?></td>

 <td><?php echo $user->nama_guru ?></td>

 <td><?php echo $user->tempat_lahir .", ". $user->tanggal_lahir ?></td>

 <td><?php echo $user->j_kelamin ?></td>

 <td><?php echo $user->alamat ?></td>

 <td><?php echo $user->no_telp ?></td>

 <td><?php echo $user->jabatan ?></td>

 </tr>

<?php $i++; } ?>

</tbody>

</table>